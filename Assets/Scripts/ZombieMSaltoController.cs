using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieMSaltoController : MonoBehaviour
{
    private Rigidbody2D rb;
    public float velocity  = -6;
    public float jump = 30;
    private SpriteRenderer sr;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

     private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Piso"))
        {
            if (velocity < 0)
            {
                sr.flipX = true;
            }else
            {
                sr.flipX = false;
            }
            rb.velocity = new Vector2(velocity, rb.velocity.y);
            rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
            velocity = velocity * -1;
            
        }

    }
}
