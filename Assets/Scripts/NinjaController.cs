using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaController : MonoBehaviour
{
   
   
    private Rigidbody2D rb;
    private Animator animator;
    public float velocity  = 6;
    public float jump = 30;
    public bool live = true;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (live)
        { 
            animator.SetInteger("Estado", 0);
            
            rb.velocity = new Vector2(velocity, rb.velocity.y);

            if (Input.GetKeyUp(KeyCode.Space))
            {
                rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
                animator.SetInteger("Estado",1); 
            }   
        }else
        {
            animator.SetInteger("Estado", 2);
            
            rb.velocity = new Vector2(0, rb.velocity.y);

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            live = false;
        }
       

    }
}
