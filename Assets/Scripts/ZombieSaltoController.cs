using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSaltoController : MonoBehaviour
{
    private Rigidbody2D rb;
    public float jump = 40;
    void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("Piso"))
        {
            rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
        }
        

    }
}
